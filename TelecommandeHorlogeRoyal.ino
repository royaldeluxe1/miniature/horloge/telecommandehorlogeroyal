
/* Programme Boitier commande Horloge Royal de luxe


    Potetiomètre 4,7K vitesse sens horaire       A0
    Potetiomètre 4.7K vitesse sens antihoraire   A1

*/

#include <SoftwareSerial.h>
#define rxPin 2
#define txPin 3

SoftwareSerial mySerial =  SoftwareSerial(rxPin, txPin);

const int sensHoraire = A2 ;
const int sensAntiHoraire = A3;
const int pinBouton = 11;  // mise à midi retour sens antihoraire
const int pinBouton2 = 12; // mise à midi sens horaire
const int ledSeconde = 13;
int ledState = LOW;             // ledState used to set the LED

//unsigned long previousMillis = 0;        // will store last time
//unsigned long previousMillisAffTemps = 0;
unsigned long currentMillisled;
unsigned long previousMillisLed = 0;    // sauvegarde last time etat led en 13


int seconde = 0;
int minute = 0;
int heure = 0;
int mouvHeure = 0;



void setup() {

  Serial.begin(9600);
  mySerial.begin(9600);

  pinMode (sensHoraire, INPUT_PULLUP);
  pinMode (sensAntiHoraire, INPUT_PULLUP);
  pinMode (pinBouton, INPUT_PULLUP);
  pinMode (pinBouton2, INPUT_PULLUP);

  pinMode (ledSeconde, OUTPUT);

}

void loop() {
  //aff_Temps ();

  //**********************  retour à midi antihoraire

  if ( !digitalRead (pinBouton) ) {
    String x = String ("r ");
    envoyer (x , 1);
    delay (100);
    //envoyer (x , 1);

    while ( !digitalRead (pinBouton)) {
      ledClignote ();
    }
  }

  //**********************  retour à midi sens horaire

  else if ( !digitalRead (pinBouton2)) {
    String x = String ("r ");
    envoyer (x , -1);
    delay (100);
    //envoyer (x , -1);

    while (!digitalRead (pinBouton2)) {
      ledClignote ();
    }
  }


  //**********************  inter en position sens Horaire

  else if ( !digitalRead(sensHoraire) && digitalRead(sensAntiHoraire)) {

    int potHoraire = analogRead (A0);
    potHoraire = constrain (potHoraire, 0, 1022);//  sensVal = constrain(sensVal, 10, 150);

    String x = String ("h ");
    envoyer ( x, potHoraire);
    if (100 <= potHoraire & potHoraire <= 150) {
      digitalWrite(ledSeconde, HIGH);
    }
    else {
      digitalWrite(ledSeconde, LOW);
    }
  }

  //*******************************  Inter en position Anti Horaire

  else if ( digitalRead(sensHoraire) && !digitalRead(sensAntiHoraire)) {

    int potAntiHoraire = analogRead (A1);
    potAntiHoraire = constrain (potAntiHoraire, 0, 1022);

    digitalWrite(ledSeconde, LOW);

    String x = String ("a ");
    envoyer ( x, potAntiHoraire);

  }


  //*******************************   inter en position Stop

  else if ( digitalRead(sensHoraire) && digitalRead(sensAntiHoraire)) {

    int potHoraire = analogRead (A0);
    if (100 <= potHoraire & potHoraire <= 150) {
      digitalWrite(ledSeconde, HIGH);
    }
    else {
      digitalWrite(ledSeconde, LOW);
    }

    String x = String ("s ");
    envoyer ( x, 0);

  }


  delay (100);
}
